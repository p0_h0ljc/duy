<?php 

/**
* 
*/
define("ADMIN", 0);
define("USER", 1);
define("GUEST", 2);

class MY_Controller extends CI_Controller
{

	function __construct($user_type)
	{
		parent::__construct();
		$this->load->helper("url");
		$this->load->library("session");

		if ($user_type == ADMIN) {
			if (!$this->isAdmin()) {
				redirect('admin', 'location', 301);
			}
		}
	}

	private function isAdmin() {
		return isset($_SESSION['logged_in'])
		&& ($this->session->userdata('logged_in') == TRUE)
		&& $this->session->userdata('user_type') == ADMIN;
		
	}


}

?>