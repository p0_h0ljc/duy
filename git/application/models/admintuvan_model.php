<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class admintuvan_model extends CI_Model {
 public function __construct()
	 {
	  parent::__construct();
	  $this->load->database();
	 }
	 function get($id = null) {
		if (is_null($id)) {
			// get all
			$this->db->select("id, title, description, upload_date");
			$this->db->from("newd");
			$this->db->order_by('upload_date','DESC');
			$offset=$this->uri->segment(3);
			$limit = 12;
			$this->db->limit($limit,$offset);
			$query = $this->db->get();
			return $query->result_array();
		}
		$this->db->from("newd");
		$this->db->where("id", $id);
		$q = $this->db->get();
		return $q->result_array();
	}
	function delete($newd) {
		foreach ($newd as $id) {
			$this->db->or_where("id", $id);
	}

		$this->db->delete("newd");
	}
	function update($new) {
		$this->insert($new, true);
		
	}
	function insert($new, $isUpdate = false) {
		
		if ($isUpdate) {
			$this->db->from("newd");
			$this->db->where("id", $new["id"]);
			$q = $this->db->get();
			if ($q->num_rows() > 0) {
				$this->db->where("id", $new["id"]);
				if (!$this->db->update("newd", $new)) {
					return -1;
				}
				return $new["id"];
			}
		} else {
			if (!$this->db->insert('newd', $new)) {
				return -1;
			}
			return $this->db->insert_id();
		}
	
	}
}
?>