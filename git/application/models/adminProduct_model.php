<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class adminProduct_model extends CI_Model {
 public function __construct()
	 {
	  parent::__construct();
	  $this->load->database();
	 }
	 function get($id = null) {
		if (is_null($id)) {
			// get all
			$this->db->select("id, dangcatmai, kichthuoc, trongluong, dotinhkhiet");
			$this->db->from("product");
			$this->db->order_by('id');
			$offset=$this->uri->segment(3);
			$limit = 12;
			$this->db->limit($limit,$offset);
			$query = $this->db->get();
			return $query->result_array();
		}
		$this->db->from("product");
		$this->db->where("id", $id);
		$q = $this->db->get();
		return $q->result_array();
	}
 public function add_product()
	 {
	  $data=array(
	    'dangcatmai'=>$this->input->post('dangcatmai'),
	    'kichthuoc'=>$this->input->post('kichthuoc'),
	  	'trongluong'=>$this->input->post('trongluong'),
	    'dotinhkhiet' => $this->input->post('dotinhkhiet'),
	    'capdomau' => $this->input->post('capdomau'),
	    'docatmai' => $this->input->post('docatmai'),
	    'chieuday'=> $this->input->post('chieuday'),
	    'chieurongmatban'=> $this->input->post('chieurongmatban'),
	    'chieucaomattren'=> $this->input->post('chieucaomattren'),
	    'chieudaymatduoi'=> $this->input->post('chieudaymatduoi'),
	    'kichcotimday'=> $this->input->post('kichcotimday'),
	    'maibong'=> $this->input->post('maibong'),
	    'doixung'=> $this->input->post('doixung'),
	    'phatquang' => $this->input->post('phatquang'),
	    'description' => $this->input->post('description')

	  );
	  $this->db->insert('product',$data);
	  // $this->db->reset();
	 }
	function delete($productIds) {
		foreach ($productIds as $id) {
			$this->db->or_where("id", $id);
	}

		$this->db->delete("product");
	}
	function update($product) {
		$this->insert($product, true);
		
	}
	function insert($product, $isUpdate = false) {
		
		if ($isUpdate) {
			$this->db->from("product");
			$this->db->where("id", $product["id"]);
			$q = $this->db->get();
			if ($q->num_rows() > 0) {
				$this->db->where("id", $product["id"]);
				if (!$this->db->update("product", $product)) {
					return -1;
				}
				return $product["id"];
			}
		} else {
			if (!$this->db->insert('product', $product)) {
				return -1;
			}
			return $this->db->insert_id();
		}
	
	}
}
?>