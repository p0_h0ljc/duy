<?php 

/**
* 
*/
class AdminUser_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	/**
	* @param type 0 is admin, 1 is user, 2 is guest
	*/
	function get($type, $id=null) {
		if ($type == USER) {
			$this->db->select("id, username, first_name, last_name, address, phone_number, email, user_type");
		} else if ($type == GUEST) {
			$this->db->select("id, first_name, last_name, address, phone_number, email, user_type");
		}

		$this->db->where("user_type", $type);
		$this->db->from("user");
		if (!is_null($id)) {
			$this->db->where("id", $id);
		}

		$q = $this->db->get();

		return $q->result_array();
	} 

	function delete($ids = null) {
		if (is_null($ids)) return false;
		foreach ($ids as $id) {
			$this->db->or_where("id", $id);	
		}
		
		if ($this->db->delete("user") ) return true;
		return false;

	}
}