<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class login_model extends CI_Model {
 public function __construct()
	 {
	  parent::__construct();
	  $this->load->database();
	 }
 function login($username,$password)
	 {
	  $this->db->where("username",$username);
	  $query=$this->db->get("user");
	  $passhash='';
	  if($query->num_rows()>0){
 		$passhash =  $query->result_array()[0]['password'];
 		 $newdata = array(
	      'user_id'  => $query->result_array()[0]['id'],
	      'user_name'  =>$query->result_array()[0]['username'],
	      'user_type' =>$query->result_array()[0]['user_type'],
	      'logged_in'  => TRUE,
	    );
 	  }
 	  $result=password_verify($password,$passhash);
	   if($result){ 
	   $this->session->set_userdata($newdata);
	   return TRUE;
		}
	   else return FALSE;
	 }
 // public function add_user()
	//  {
	//   $data=array(
	//     'username'=>$this->input->post('user_name'),
	//     'email'=>$this->input->post('email_address'),
	//     'password'=>password_hash($this->input->post('password'),PASSWORD_DEFAULT),
	//     'first_name' => $this->input->post('firstname'),
	//     'last_name' => $this->input->post('lastname'),
	//     'phone_number'=> $this->input->post('phone'),
	//     'user_type'=>'1',
	//     'address' => $this->input->post('address')
	//   );
	//   $this->db->insert('user',$data);
	//  }
}
?>