<?php 

/**
* 
*/
class admingetFiles_model extends CI_Model
{
	function __construct()
	{
		$this->load->database();
		$this->load->dbutil();
	}
	function get($id = null) {
		if (is_null($id)) {
			// get all
			$this->db->select("file_path,file_name,title,upload_date");
			$this->db->from("files");
			$this->db->order_by('upload_date','DESC');
			$offset=$this->uri->segment(3);
			$limit = 12;
			$this->db->limit($limit,$offset);
			$query = $this->db->get();
			return $query->result_array();
		}
		$this->db->from("files");
		$this->db->where("file_id", $id);
		$q = $this->db->get();
		return $q->result_array();

	}
}

?>