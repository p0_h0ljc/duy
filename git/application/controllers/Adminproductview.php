<?php 

/**
* 
*/
class adminproductview extends MY_Controller
{
	function __construct()
	{
		parent::__construct(ADMIN);
		$this->load->helper("url");
		$this->load->model("adminProduct_model");
		$this->load->library('pagination');
	}
	function index() {
		$config['base_url'] = site_url() . '/adminproductview/index/';
		$data["products"] = $this->adminProduct_model->get();
		$config['total_rows'] = $this->db->count_all('product');
        $config['uri_segment']  = 3;
        $config['per_page'] = 12;
        $config['prev_link']  = '&lt;';
        $config['next_link']  = '&gt;';
        $config['last_link']  = 'Cuối';
        $config['first_link'] = 'Đầu';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $paginator=$this->pagination->create_links(); 
       	$data['paginator']=$paginator;
		$this->load->view("template/admin/sidebar");
		$this->load->view("admin/products", $data);
		$this->load->view("template/admin/footer");
	}
	function update() {
		$product = $_POST;
		unset($product["images"]);
		$this->adminProduct_model->update($product);
		redirect('admin/product/' . $product["id"], 'refresh');
	}

	function delete() {
		if (!isset($_POST["selected"])) redirect('adminproductview', 'refresh');;
		$this->adminProduct_model->delete($_POST["selected"]);
		redirect('adminproductview', 'refresh');
	}

	function view($id) {
		$data = $this->adminProduct_model->get($id)[0];
		$data["description"] = html_escape($data["description"]);
		$data["images"] = explode(",", $data["images"]);
		$this->load->view("template/admin/sidebar");
		$this->load->view("admin/product_detail", $data);
		$this->load->view("template/admin/footer");
	}
}