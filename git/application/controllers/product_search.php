<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class product_search extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper("url");
		$this->load->model("adminProduct_model");
	}
	public function index()
	{
		
	}
	public function search($id){
		$data = $this->adminProduct_model->get($id)[0];
		$data["description"] = html_escape($data["description"]);

		$this->load->view("template/header");
		$this->load->view("product_view", $data);
		$this->load->view("template/footer");
	}
	public function render($id) {	
		// Load all views as normal
		$data = $this->adminProduct_model->get($id)[0];
		$this->load->view('product_view',$data);
		// Get output html
		$html = $this->output->get_output();
	
		// Load library
		$this->load->library('dompdf_gen');
		
		// Convert to PDF
		$this->dompdf->load_html(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
		$this->dompdf->render();
		$this->dompdf->stream("welcome.pdf");
		redirect("product_view",$data);
	}
}
