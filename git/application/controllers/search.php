	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Search extends CI_controller{ 
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('url','form'));
		$this->load->model("adminProduct_model");
	}
	function search(){
		$search=$this->input->get('search');
		$data = $this->adminProduct_model->get($search)[0];
		if (isset($data)) {
			redirect('product_search/search/' . $data["id"], 'refresh');
		}
		else
			redirect('/homepage');
		}
}	