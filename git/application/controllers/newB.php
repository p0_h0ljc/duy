<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class newB extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper("url");
		$this->load->model("adminNew_model1");
		$this->load->library('pagination');

	}
	public function index()
	{
		$config['base_url'] = site_url() . '/newb/index/';
		$listnew = $this->adminNew_model1->get();
		$data['listnew']=$listnew;
		$config['total_rows'] = $this->db->count_all('newb');
        $config['uri_segment']  = 3;
        $config['per_page'] = 12;
        $config['prev_link']  = '&lt;';
        $config['next_link']  = '&gt;';
        $config['last_link']  = 'Cuối';
        $config['first_link'] = 'Đầu';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $paginator=$this->pagination->create_links(); 
       	$data['paginator']=$paginator;
		$this->load->view("template/header");
		$this->load->view("view_new",$data);
		$this->load->view("template/footer");
	}
	public function new($id){
		$data = $this->adminNew_model1->get($id)[0];
		$this->load->view("template/header");
		$this->load->view("new",$data);
		$this->load->view("template/footer");
	}
}
