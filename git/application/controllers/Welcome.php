<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data = $this->adminProduct_model->get($id)[0];
		$data["description"] = html_escape($data["description"]);
		$this->load->view("template/header");
		$this->load->view("product_view", $data);
		$this->load->view("template/footer");
	}
	Public function generate_pdf(){

$this->load->library("mypdf");

$html = $this->load->view("welcome");

$this->dompdf->load_html($html);

$this->dompdf->set_paper("A4", "portrait"); // to change page orientation to landscape, change the parameter “portrait” to “landscape”

$this->dompdf->render();

$filename = "mypdf.pdf";

$this->dompdf->stream("$filename"); 

}
}
