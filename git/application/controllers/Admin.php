<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct()
	{
		parent::__construct(ADMIN);
		$this->load->helper("url");
	}
	public function index()
	{	
		$this->dashboard();
	}
	public function dashboard() {
		$this->load->view("template/admin/sidebar");
		$this->load->view("admin/dashboard");
		$this->load->view("template/admin/footer");
	}

}
