<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends CI_Controller {
	function __construct()
	{
		parent::__construct(ADMIN);
		$this->load->helper("url");
		$this->load->helper("download");
		$this->load->model("admingetFiles_model");
		$this->load->library('pagination');

	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		$config['base_url'] = site_url() . '/download/index/';
		$data['title']='download';
		$listfiles= $this->admingetFiles_model->get();
		$data["listfiles"]= $listfiles;
		$config['total_rows'] = $this->db->count_all('files');
        $config['uri_segment']  = 3;
        $config['per_page'] = 12;
        $config['prev_link']  = '&lt;';
        $config['next_link']  = '&gt;';
        $config['last_link']  = 'Cuối';
        $config['first_link'] = 'Đầu';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $paginator=$this->pagination->create_links(); 
       	$data['paginator']=$paginator;
	 	$this->load->view('template/header1',$data);
	 	// var_dump($data["files"]);
	 	$this->load->view('download_view',$data);
	 	$this->load->view('template/footer');
	}	
	function download(){
    $this->load->helper('download');
    $data = file_get_contents('upload/'.$this->uri->segment(3)); // Read the file's contents
    $name = $this->uri->segment(3);
    force_download($name, $data);
}
}
