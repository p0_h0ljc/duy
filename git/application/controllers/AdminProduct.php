<?php 

/**
* 
*/
class AdminProduct extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct(ADMIN);
		$this->load->helper("url");
		$this->load->model("adminProduct_model");
	}

	function index() {
		$this->load->view("template/admin/sidebar");
		$this->load->view("admin/add_product");
		$this->load->view("template/admin/footer");
	}
	function view($id) {
		$data = $this->adminProduct_model->get($id)[0];
		$data["description"] = html_escape($data["description"]);
		$this->load->view("template/admin/sidebar");
		$this->load->view("admin/product_detail", $data);
		$this->load->view("template/admin/footer");
	}
	function viewproduct($id){
		$data = $this->adminProduct_model->get($id)[0];
		$data["description"] = html_escape($data["description"]);
		$this->load->view("template/header");
		$this->load->view("product_view", $data);
		$this->load->view("template/footer");
	}
		function update() {
		$product = $_POST;
		$this->adminProduct_model->update($product);
		redirect('admin/product/' . $product["id"], 'refresh');
	}
	public function add()
		 {
		  $this->load->library('form_validation');
		  // field name, error message, validation rules
		  $this->form_validation->set_rules('dangcatmai', 'Dang Cat Mai', 'trim|required');
		  $this->form_validation->set_rules('kichthuoc', 'Kich Thuoc', 'trim|required');
		  $this->form_validation->set_rules('trongluong', 'Trong Luong', 'trim|required');
		  $this->form_validation->set_rules('dotinhkhiet', 'Do Tinh Khiet', 'trim|required');
		  $this->form_validation->set_rules('capdomau', 'Cap Do Mau', 'trim|required');
		  $this->form_validation->set_rules('docatmai', 'Do Cat Mai', 'trim|required');
		  $this->form_validation->set_rules('chieuday', 'Chieu Day', 'trim|required');
		  $this->form_validation->set_rules('chieurongmatban', 'Chieu Rong Mat Ban', 'trim|required');
		  $this->form_validation->set_rules('chieucaomattren', 'Chieu Cao Mat Tren', 'trim|required');
		  $this->form_validation->set_rules('chieudaymatduoi', 'Chieu Day Mat Duoi', 'trim|required');
		  $this->form_validation->set_rules('kichcotimday', 'Kich Co Tim Day', 'trim|required');
		  $this->form_validation->set_rules('maibong', 'Mai Bong', 'trim|required');
		  $this->form_validation->set_rules('doixung', 'Doi Xung', 'trim|required');
		  $this->form_validation->set_rules('phatquang', 'Phat Quang', 'trim|required');		  
		  $this->form_validation->set_rules('description', 'Mo Ta', 'trim');		  
		  // $this->form_validation->set_rules('email_address', 'Your Email', 'trim|required|valid_email');
		  // $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
		  // $this->form_validation->set_rules('con_password', 'Password Confirmation', 'trim|required|matches[password]');
		  // $this->form_validation->set_rules('firstname', 'First Name', 'trim|alpha|required');
		  // $this->form_validation->set_rules('lastname', 'Last Name', 'trim|alpha|required');
		  // $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[10]|max_length[13]');
		  if($this->form_validation->run() == FALSE)
		  {
		   $this->index();
		  }
		  else
		  {
		   $this->adminProduct_model->add_product();
		   $this->index();
		  }
 		}

	}