<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {
	public function __construct()
	{
		  parent::__construct();
		  $this->load->model('login_model');
		  $this->load->library('session');
		  $this->load->helper(array('url','form'));
		  $this->load->database();
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		$data['title']='login';
		$this->load->view('template/header',$data);
		$this->load->view('login_view');
		$this->load->view('template/footer');
	}
	public function login(){
		
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$result = $this->login_model->login($username,$password);
		if($result) {
			redirect('admin/dashboard');
		}
		else $this->index();
	}
	public function logout()
	 {
	  $this->session->unset_userdata(array('user_id', 'user_name', 'user_email', 'user_type', 'logged_in'));
	  redirect('homepage');
	 }
}
