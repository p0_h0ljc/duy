<?php 
/**
* 
*/


class AdminUser extends My_Controller
{

	function __construct()
	{
		parent::__construct(ADMIN);
		$this->load->helper("url");
		$this->load->model("adminUser_model");
	}

	function user() {
		$data["users"] = $this->adminUser_model->get(USER);
		$this->loadView($data);
	}

	function guest() {
		$data["guests"] = $this->adminUser_model->get(GUEST);
		$this->loadView($data);
	}

	private function loadView($data) {
		$this->load->view("template/admin/sidebar");
		$this->load->view("admin/users", $data);
		$this->load->view("template/admin/footer");
	}

	function delete() {
		var_dump($this->uri->uri_string());
		$segments = explode("/", rtrim($this->uri->uri_string(), "/"));
		var_dump($segments);
		$segments = array_slice($segments, 0, count($segments) - 1);
		$uri = implode("/", $segments);
		
		if (!isset($_POST['selected'])) {
			redirect($uri);
			return;
		}

		$this->adminUser_model->delete($_POST['selected']);
		redirect($uri);
	}
}