<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class adminnewb extends MY_Controller {
		function __construct()
	{
		parent::__construct(ADMIN);
		$this->load->helper("url");
		$this->load->model("adminNew_model1");
		$this->load->library('pagination');
	}
	function index() {
		$config['base_url'] = site_url() . '/adminewa/index/';
		$data["news"] = $this->adminNew_model1->get();
		$config['total_rows'] = $this->db->count_all('newb');
        $config['uri_segment']  = 3;
        $config['per_page'] = 12;
        $config['prev_link']  = '&lt;';
        $config['next_link']  = '&gt;';
        $config['last_link']  = 'Cuối';
        $config['first_link'] = 'Đầu';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $paginator=$this->pagination->create_links(); 
       	$data['paginator']=$paginator;
		$this->load->view("template/admin/sidebar");
		$this->load->view("postnew_view", $data);
		$this->load->view("template/admin/footer");
	}
	function add() {
		$this->load->view("template/admin/sidebar");
		$this->load->view("addnew_view.php");
		$this->load->view("template/admin/footer");

	}
	private function do_upload($id) {
		$path = './assets/images/products/' . $id;
		if (!is_dir($path)) mkdir($path, 0777, TRUE);

		$config['upload_path']   = $path; 
        $config['allowed_types'] = 'jpg|png|svg'; 
        $config['max_size']      = 100; 
        $config['max_width']     = 1024; 
        $config['max_height']    = 1024;  
        $this->load->library('upload', $config);
        $images = [];
        $files = $_FILES;
        $cpt = count($_FILES['images']['name']);
        for($i=0; $i<$cpt; $i++) {
	        $_FILES['images']['name']= $files['images']['name'][$i];
	        $_FILES['images']['type']= $files['images']['type'][$i];
	        $_FILES['images']['tmp_name']= $files['images']['tmp_name'][$i];
	        $_FILES['images']['error']= $files['images']['error'][$i];
	        $_FILES['images']['size']= $files['images']['size'][$i];
	        if ( ! $this->upload->do_upload('images')) {
	           	$error = array('error' => $this->upload->display_errors()); 
	           	var_dump($error);
	        }
	        else { 
	           	$data = array('upload_data' => $this->upload->data()); 
	           	//$this->load->view('upload_success', $data); 
	           	//var_dump($data);
	           	$fileName = $_FILES['images']['name'];
       			$images[] = $fileName;
	        }
	        
		}
        return $images;
	}

	function addnew() {
		$data = $_POST;
		unset($data["images"]);
		$id = $this->adminNew_model->insert($data);
		if ( $id != -1 ) {
			$data["id"] = $id;
			$this->adminNew_model->update($data);
		}
		redirect('adminNewA', 'refresh');	
	}
	function update() {
		$new = $_POST;
		$this->adminNew_model->update($new);
		redirect('adminnewA/view/' . $new["id"], 'refresh');
	}
	function delete() {
		if (!isset($_POST["selected"])) redirect('adminNewA', 'refresh');
		$this->adminNew_model->delete($_POST["selected"]);
		redirect('adminNewA', 'refresh');
	}
	function view($id) {
		$data = $this->adminNew_model->get($id)[0];
		
		$this->load->view("template/admin/sidebar");
		$this->load->view("viewdetail_view", $data);
		$this->load->view("template/admin/footer");
	}
}
