<?php
header( 'Cache-Control: no-store, no-cache, must-revalidate' ); 
header( 'Cache-Control: post-check=0, pre-check=0', false ); 
header( 'Pragma: no-cache' ); 
?>
	<div class="right_col">
		<div class="right-container">
			<div class="box">
				<div class="box-heading">
					<i class="fa fa-cog"></i>
					<h3>Đăng Sản Phẩm</h3>
				</div>
				<div class="box-body">
					<?php echo validation_errors('<p class="error">'); ?>
 			<form class="form-horizontal" enctype="multipart/form-data" id="form" action="<?php echo base_url('adminproduct/add') ?>" method="post">
  
  <div class="form-group">
              <label class="control-label col-xs-4 col-sm-2" for="dangcatmai">Dạng cắt mài</label>
            <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="text" id="dangcatmai" name="dangcatmai">
              </div>
              
            </div>
            <div class="form-group">
              <label class="control-label col-xs-4 col-sm-2" for="kichthuoc">Kích thước</label>
              <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="text" id="kichthuoc" name="kichthuoc">
              </div>
              
            </div>
            <div class="form-group">
              <label class="control-label col-xs-4 col-sm-2" for="trongluong">Trọng Lượng </label>
             <div class="col-xs-4 col-sm-4">
                <input class="form-control"  type="text" id="trongluong" name="trongluong">
              </div>
              
            </div>
            <div class="form-group">
              <label class="control-label col-xs-4 col-sm-2" for="dotinhkhiet">Độ Tinh Khiết </label>
              <div class="col-xs-4 col-sm-4">
                <input class="form-control"  type="text" id="dotinhkhiet" name="dotinhkhiet">
              </div>
              
            </div>
            
            
            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="capdomau">Cấp Độ Màu </label>
              <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="text" id="capdomau" name="capdomau">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="docatmai">Độ Cắt Mài</label>
              <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="text" id="docatmai" name="docatmai">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="chieuday">Chiều Dầy </label>
              <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="number" id="chieuday" name="chieuday">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="chieurongmatban">Chiều Rộng Mặt Bàn </label>
            <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="number" id="chieurongmatban" name="chieurongmatban">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="chieucaomattren">Chiều Cao Mặt Trên </label>
              <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="number" id="chieucaomattren" name="chieucaomattren">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="chieudaymatduoi">Chiều Dầy Mặt Dưới </label>
              <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="number" id="chieudaymatduoi" name="chieudaymatduoi">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="kichcotimday">Kích Cỡ Tim Đáy</label>
             <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="text" id="kichcotimday" name="kichcotimday">
              </div>
            </div>
            <div class="form-group" style="margin-left:50px;">
            <strong>Mài Bóng</strong>
              <select id="maibong" name="maibong">
              <option value="Very Good" selected>Very Good</option>
                <option value="Good">Good</option>
                <option value="Medium">Medium</option>
                <option value="Poor">Poor</option>
                <option value="None">None</option>
              </select>
            </div>
            <div class="form-group" style="margin-left:50px;">
            <strong>Đối Xứng</strong>
              <select id="doixung" name="doixung">
                <option value="Very Good" selected>Very Good</option>
                <option value="Good">Good</option>
                <option value="Medium">Medium</option>
                <option value="Poor">Poor</option>
                <option value="None">None</option>
              </select>
            </div>
            <div class="form-group" style="margin-left:50px;" >
            <strong> Phát Quang</strong>
              <select id="phatquang" name="phatquang">
                <option value="Very Good" selected>Very Good</option>
                <option value="Good">Good</option>
                <option value="Medium">Medium</option>
                <option value="Poor">Poor</option>
                <option value="None">None</option>
                
              </select>
            </div>
            	<div class="form-group">
							<label class="control-label col-xs-12 col-sm-2" for="description">Description: </label>
							<textarea cols="50" rows="4" name="description" id="description"></textarea>
							</div>
						
  <p>
  <input type="submit" class="btn btn-danger" value="Submit" />
  <input type="reset" class="btn btn-danger" value="reset" />
  </p>
 </form>
				</div>
			</div>

		</div>
		
	</div>
<script src="/vendors/summernote/dist/summernote.min.js"></script>
<script type="text/javascript">

	$(document).ready(function() {

	    // summernote
	    $('#summernote').summernote({
	    	height: 200,
	    	minHeight: 200,
	    	maxHeight: 800,
	    	focus: false,
	    	placeholder: 'Description here'
	    });
	});


	function add() {
		var values = [];
		var isHasError = false;

		$('#form').find('span.errors').remove();

		// set data for input description
		$('#form').find('input[name="description"]').val($('#summernote').summernote('code'));

		$.each($('#form').serializeArray(), function(i, field) {
			
			switch (field.name) {
				case 'name': console.log(field);
					if (field.name == 'name' && field.value.length < 5) {
						$('#form').find('input[name="name"]').parent().parent().append("<span class='errors'>The length of product name must be greater than 5 characters</span>");
						isHasError = true;
					}
					break;

				case 'price': console.log(field);
					if ( isNaN(parseFloat(field.value)) || field.value < 0) {
						$('#form').find('input[name="price"]').parent().parent().append("<span class='errors'>Price of product must be a number greater 0</span>");
						isHasError = true;
					}
					break;

				case 'quantity': console.log(field);
					if (parseInt(field.value) < 0) {
						$('#form').find('input[name="quantity"]').parent().parent().append("<span class='errors'>Quantity must be greater 0</span>");
						isHasError = true;
					}
					break;

				case 'brand_id': 
					if (field.value == '') {
						$('#form').find('select[name="brand_id"]').parent().parent().append("<span class='errors'>Please choose a brand</span>");
						isHasError = true;
					}
					break;

				default: 
					console.log("");
					break;
			}
			
		});
		

		if (!isHasError) {
			return true;
		}

		return false;
	}

</script>
