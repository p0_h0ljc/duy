
	<div class="right_col">
		<div class="right-container">
			<div class="box">
				<div class="box-heading">
					<i class="fa fa-cog"></i>
					<h3>Sản Phẩm</h3>
					<div class="buttons">
						<a href="<?php echo base_url('/admin/product/add'); ?>" class="btn btn-sm btn-primary">Insert</a>
						<button class="btn btn-sm btn-danger" onclick="$('#form').submit()">Delete</button>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="box-body">
					<form id="form" action="<?php echo base_url('adminproductview/delete') ?>" method="post">
						<table class="table table-responsive table-bordered table-hover list_table ">
						<thead>
							<tr>
								<td>
									<input type="checkbox" onchange="$('input[name*=\'selected\']').prop('checked', this.checked);">
								</td>
								<td>Số</td>
								<td>Dạng cắt mài</td>
								<td>Kích Thước</td>
								<td>Trọng Lượng</td>
								<td>Chỉnh sửa</td>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($products as $product) { 
							
							?>
								<tr>
									<td>
										<input type="checkbox" value="<?php echo $product['id']; ?>" name="selected[]">
									</td>
									<td ><?php echo $product["id"] ?></td>
									<td>
										<?php echo $product["dangcatmai"] ?>
									</td>
									<td class="left"><?php echo $product["kichthuoc"]; ?></td>
									<td class="right"><?php echo $product["trongluong"]; ?></td>
									<td class="center edit_link">
										<a href="<?php echo base_url('admin/product/' . $product["id"]); ?>">Edit</a>
									</td>
								</tr>
								
							<?php } ?>
							
						</tbody>
					</table>
					</form>
					<div class="row">
						        <div class="col-md-12 text-center">
						            <?php echo $paginator; ?>
						        </div>
						    </div>
				</div>
			</div>

		</div>
		
	</div>
<script type="text/javascript">
	
</script>
