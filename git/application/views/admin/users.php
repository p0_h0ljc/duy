<?php 
$isUserType = isset($users);
$data = $isUserType ? $users : $guests;
$action = $isUserType ? base_url('admin/users/delete') : base_url('admin/guests/delete');
?>

<div class="right_col">
	<div class="right-container">
		<div class="box">
			<div class="box-heading">
				<i class="fa fa-cubes"></i>
				<h3>Users</h3>
				<div class="buttons">
					<button class="btn btn-sm btn-danger" onclick="$('#form').submit()">Delete</button>
				</div>			</div>
			<div class="box-body">
				<form id="form" action="<?php echo $action ?>" method="post">
					<table class="table table-responsive table-bordered table-hover list_table ">
						<thead>
							<tr>
								<td><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);"></td>
								<td>ID</td>
								<?php if ($isUserType) {?> 
								<td>Username</td>
								<?php } ?>
								<td>Full Name</td>
								<td>Phone Number</td>
								<td>Email</td>
								<td>Address</td>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($data as $obj): ?>
								
							<tr>
								<td class="center"><input type="checkbox" name="selected[]" value="<?php echo $obj["id"]; ?>"></td>
								<td class="center">
									<?php echo $obj["id"]; ?>
								</td>
								<?php if ($isUserType) {?> 
								<td><?php echo $obj['username'] ?></td>
								<?php } ?>
								<td class="left">
									<?php echo $obj["first_name"]." ".$obj["last_name"]; ?>
								</td>
								<td class="right">
									<?php echo $obj["phone_number"]; ?>
								</td>
								<td class="center">
									<?php echo $obj["email"]; ?>
								</td>
								<td class="right">
									<?php echo $obj["address"]; ?>
								</td>
							</tr>
							<?php endforeach ?>
							
						</tbody>
					</table>
				</form>
			</div>
		</div>

	</div>
	
</div>