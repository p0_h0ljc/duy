
	<div class="right_col">
		<div class="right-container">
			<div class="box">
				<div class="box-heading">
					<i class="fa fa-cog"></i>
					<h3>Thông Tin Sản Phẩm</h3>
				</div>
				<div class="box-body">
					<form class="form-horizontal" enctype="multipart/form-data" id="form" action="<?php echo base_url('adminproduct/update') ?>" method="post">
          <input type="hidden" name="id" value="<?php echo $id ?>">
            <div class="form-group">
              <label class="control-label col-xs-4 col-sm-2" for="dangcatmai">Dạng cắt mài</label>
            <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="text" id="dangcatmai" name="dangcatmai" value="<?php echo $dangcatmai;  ?>">
              </div>
              
            </div>
            <div class="form-group">
              <label class="control-label col-xs-4 col-sm-2" for="kichthuoc">Kích thước</label>
              <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="text" id="kichthuoc" name="kichthuoc" value="<?php echo $kichthuoc; ?>">
              </div>
              
            </div>
            <div class="form-group">
              <label class="control-label col-xs-4 col-sm-2" for="trongluong">Trọng Lượng </label>
             <div class="col-xs-4 col-sm-4">
                <input class="form-control"  type="text" id="trongluong" name="trongluong" value="<?php echo $trongluong; ?>">
              </div>
              
            </div>
            <div class="form-group">
              <label class="control-label col-xs-4 col-sm-2" for="dotinhkhiet">Độ Tinh Khiết </label>
              <div class="col-xs-4 col-sm-4">
                <input class="form-control"  type="text" id="dotinhkhiet" name="dotinhkhiet" value="<?php echo $dotinhkhiet; ?>">
              </div>
              
            </div>
            
            
            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="capdomau">Cấp Độ Màu </label>
              <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="text" id="capdomau" name="capdomau" value="<?php echo $capdomau; ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="docatmai">Độ Cắt Mài</label>
              <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="text" id="docatmai" name="docatmai" value="<?php echo $docatmai; ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="chieuday">Chiều Dầy </label>
              <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="number" id="chieuday" name="chieuday" value="<?php echo $chieuday; ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="chieurongmatban">Chiều Rộng Mặt Bàn </label>
            <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="number" id="chieurongmatban" name="chieurongmatban" value="<?php echo $chieurongmatban; ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="chieucaomattren">Chiều Cao Mặt Trên </label>
              <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="number" id="chieucaomattren" name="chieucaomattren" value="<?php echo $chieucaomattren; ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="chieudaymatduoi">Chiều Dầy Mặt Dưới </label>
              <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="number" id="chieudaymatduoi" name="chieudaymatduoi" value="<?php echo $chieudaymatduoi; ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="kichcotimday">Kích Cỡ Tim Đáy</label>
             <div class="col-xs-4 col-sm-4">
                <input class="form-control" type="text" id="kichcotimday" name="kichcotimday" value="<?php echo $kichcotimday; ?>">
              </div>
            </div>
            <div class="form-group" style="margin-left:50px;">
            <strong>Mài Bóng</strong>
              <select id="maibong" name="maibong">
              <option value="Very Good" <?php if($maibong=="Very Good") echo "selected"?>>Very Good</option>
                <option value="Good"  <?php if($maibong=="Good") echo "selected"?>>Good</option>
                <option value="Medium" <?php if($maibong=="Medium") echo "selected"?>>Medium</option>
                <option value="Poor" <?php if($maibong=="Poor") echo "selected"?>>Poor</option>
                <option value="None" <?php if($maibong=="None") echo "selected"?>>None</option>
              </select>
            </div>
            <div class="form-group" style="margin-left:50px;">
            <strong>Đối Xứng</strong>
              <select id="doixung" name="doixung">
                <option value="Very Good" <?php if($doixung=="Very Good") echo "selected"?>>Very Good</option>
                <option value="Good" <?php if($doixung=="Good") echo "selected"?>>Good</option>
                <option value="Medium" <?php if($doixung=="Medium") echo "selected"?>>Medium</option>
                <option value="Poor" <?php if($doixung=="Poor") echo "selected"?>>Poor</option>
                <option value="None" <?php if($doixung=="None") echo "selected"?>>None</option>
              </select>
            </div>
            <div class="form-group" style="margin-left:50px;" >
            <strong> Phát Quang</strong>
              <select id="phatquang" name="phatquang">
                <option value="Very Good" <?php if($phatquang=="Very Good") echo "selected"?>>Very Good</option>
                <option value="Good" <?php if($phatquang=="Good") echo "selected"?>>Good</option>
                <option value="Medium" <?php if($phatquang=="Medium") echo "selected"?>>Medium</option>
                <option value="Poor" <?php if($phatquang=="Poor") echo "selected"?>>Poor</option>
                <option value="None" <?php if($phatquang=="None") echo "selected"?>>None</option>
                
              </select>
            </div>
            	<div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="description">Description: </label>
              <textarea cols="50" rows="4" name="description" id="description"><?php echo $description?></textarea>
              </div>
  <p>
  <input type="submit" class="btn btn-danger" value="Update" />
 
  </p>
 </form>
				</div>
			</div>

		</div>
		
	</div>
<script src="/vendors/summernote/dist/summernote.min.js"></script>
<script type="text/javascript">

	$(document).ready(function() {

	    // summernote
	    $('#summernote').summernote({
	    	height: 200,
	    	minHeight: 200,
	    	maxHeight: 800,
	    	focus: false,
	    	placeholder: 'Description here'
	    });

	    $('#summernote').summernote('code', $("#form").find("input[name='description']").val());
	});


	function add() {
		var values = [];
		var isHasError = false;

		$('#form').find('span.errors').remove();

		// set data for input description
		$('#form').find('input[name="description"]').val($('#summernote').summernote('code'));

		$.each($('#form').serializeArray(), function(i, field) {
			
			switch (field.name) {
				case 'name': console.log(field);
					if (field.name == 'name' && field.value.length < 5) {
						$('#form').find('input[name="name"]').parent().parent().append("<span class='errors'>The length of product name must be greater than 5 characters</span>");
						isHasError = true;
					}
					break;

				case 'price': console.log(field);
					if ( isNaN(parseFloat(field.value)) || field.value < 0) {
						$('#form').find('input[name="price"]').parent().parent().append("<span class='errors'>Price of product must be a number greater 0</span>");
						isHasError = true;
					}
					break;

				case 'quantity': console.log(field);
					if (parseInt(field.value) < 0) {
						$('#form').find('input[name="quantity"]').parent().parent().append("<span class='errors'>Quantity must be greater 0</span>");
						isHasError = true;
					}
					break;

				case 'brand_id': 
					if (field.value == '') {
						$('#form').find('select[name="brand_id"]').parent().parent().append("<span class='errors'>Please choose a brand</span>");
					}
					break;

				default: 
					console.log("");
					break;
			}
			
		});
		

		if (!isHasError) {
			return true;
		}

		return false;
	}

</script>
