<div class="right_col">
		<div class="right-container">
			<div class="box">
				<div class="box-heading">
					<i class="fa fa-cog"></i>
					<h3>Upload file</h3>
				</div>
				<div class="box-body">
					<?php
                if (isset($success) && strlen($success)) {
                    echo '<div class="success">';
                    echo '<p>' . $success . '</p>';
                    echo '</div>';
                }
                if (isset($errors) && strlen($errors)) {
                    echo '<div class="error">';
                    echo '<p>' . $errors . '</p>';
                    echo '</div>';
                }
                if (validation_errors()) {
                    echo validation_errors('<div class="error">', '</div>');
                }
                ?>
<div><?php
                    $attributes = array('name' => 'file_upload_form', 'id' => 'file_upload_form');
                    echo form_open_multipart($this->uri->uri_string(), $attributes);
                    ?>
<div class="form-group">
							<label class="control-label col-xs-12 col-sm-2" for="title">File title <i class="required">*</i></label>
							<div class="col-sm-6">
								<input class="form-control" type="text" id="title" name="title">
							</div>
							
</div>
<br>
<br>
<p><input name="file_name" id="file_name" readonly="readonly" type="file" /></p>
<p><input name="file_upload" value="Upload" type="submit" /></p>
<?php
                    echo form_close();
                    ?></div>

				</div>
			</div>

		</div>
		
	</div>
<script src="/vendors/summernote/dist/summernote.min.js"></script>
<script type="text/javascript">

	// $(document).ready(function() {

	//     // summernote
	//     $('#summernote').summernote({
	//     	height: 200,
	//     	minHeight: 200,
	//     	maxHeight: 800,
	//     	focus: false,
	//     	placeholder: 'Description here'
	//     });
	// });


	// function add() {
	// 	var values = [];
	// 	var isHasError = false;

	// 	$('#form').find('span.errors').remove();

	// 	// set data for input description
	// 	$('#form').find('input[name="description"]').val($('#summernote').summernote('code'));

	// 	$.each($('#form').serializeArray(), function(i, field) {
			
	// 		switch (field.name) {
	// 			case 'name': console.log(field);
	// 				if (field.name == 'name' && field.value.length < 5) {
	// 					$('#form').find('input[name="name"]').parent().parent().append("<span class='errors'>The length of product name must be greater than 5 characters</span>");
	// 					isHasError = true;
	// 				}
	// 				break;

	// 			case 'price': console.log(field);
	// 				if ( isNaN(parseFloat(field.value)) || field.value < 0) {
	// 					$('#form').find('input[name="price"]').parent().parent().append("<span class='errors'>Price of product must be a number greater 0</span>");
	// 					isHasError = true;
	// 				}
	// 				break;

	// 			case 'quantity': console.log(field);
	// 				if (parseInt(field.value) < 0) {
	// 					$('#form').find('input[name="quantity"]').parent().parent().append("<span class='errors'>Quantity must be greater 0</span>");
	// 					isHasError = true;
	// 				}
	// 				break;

	// 			case 'brand_id': 
	// 				if (field.value == '') {
	// 					$('#form').find('select[name="brand_id"]').parent().parent().append("<span class='errors'>Please choose a brand</span>");
	// 					isHasError = true;
	// 				}
	// 				break;

	// 			default: 
	// 				console.log("");
	// 				break;
	// 		}
			
	// 	});
		

	// 	if (!isHasError) {
	// 		return true;
	// 	}

	// 	return false;
	// }

</script>

