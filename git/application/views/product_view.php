
<style type="text/css">
th, td {
    border-bottom: 1px solid #ddd;
}
 tr td:first-child{
  color:#45a1de;
  
    font-family: "Avenir Next", "Helvetica Neue", Helvetica, Arial, sans-serif;
}
 tr td{
  color:#e74c3c;
    font-family: "Avenir Next", "Helvetica Neue", Helvetica, Arial, sans-serif;
 }
 table {
    border-collapse: collapse;
    width: 100%;
}
</style>
<button style="width:100%"><span style="font-size:30px"><a href="<?php echo base_url("product_search/render/$id")?>"><i class="fa fa-file-pdf-o">In PDF</i></a></span></button>
<table style="background-color:white;margin: 100px auto;text-align:center;width:50%">
<tr>
    <td>Number</td>
    <td><?php echo $id;  ?></td>


   
  </tr>
  <tr>
    <td>MÔ TẢ (DESCRIPTION)</td>  
    <td></td>  
  </tr>
  
  <tr>

    <td>Dạng cắt mài (Shape & Cut)  </td>
    <td><?php echo $dangcatmai;  ?></td>
    
  </tr>
  <tr>
    <td>Kích thước (Masasurement)</td>
    <td><?php echo $kichthuoc; ?></td>
  </tr>
  <tr>
    <td>TRỌNG LƯỢNG (CARAT WEIGHT)</td>
    <td><?php echo $trongluong; ?></td>
  </tr>
  <tr>
    <td>ĐỘ TINH KHIẾT (CLARITY GRADE)</td>
    <td><?php echo $dotinhkhiet; ?></td>
  </tr>
  <tr>
    <td>CẤP ĐỘ MÀU (COLOR GRADE)</td>
    <td><?php echo $capdomau; ?></td>
    
  </tr>
  <tr>
    <td>ĐỘ CẮT MÀI (CUT GRADE)</td>
    <td><?php echo $docatmai; ?></td>
    
  </tr>
  <tr>
    <td>Chiều dầy (Total Depth)</td>
    <td><?php echo $chieuday; ?></td>
    
  </tr>
   <tr>
    <td>Chiều cao mặt trên (Crown Height) </td>
    <td><?php echo $chieucaomattren; ?></td>
    
  </tr>
   <tr>
    <td>Chiều rộng mặt bàn (Table Size)</td>
    <td><?php echo $chieurongmatban; ?></td>
    
  </tr>
   <tr>
    <td>Chiều dầy mặt dưới (Pavilion Depth)</td>
    <td><?php echo $chieudaymatduoi; ?></td>
    
  </tr>
   <tr>
    <td>Kích cỡ tim đáy (Culet size)</td>
    <td><?php echo $kichcotimday; ?></td>
    
  </tr>
   <tr>
    <td>Mài bóng (Polish)</td>
    <td><?php echo $maibong?></td>
    
  </tr>
   <tr>
    <td>Đối xứng (Symmetry)</td>
    <td><?php echo $doixung?></td>
    
  </tr>
  <tr>
    <td>Phát quang (Fluorescence)</td>
    <td><?php echo $phatquang?></td>
    
  </tr>
  <tr>
    <td>CHÚ THÍCH (COMMENTS)  </td>
    <td><?php echo $description?></td>
    
  </tr>
  <tr><td>LƯU Ý QUAN TRỌNG
Bảng tham khảo này chỉ nhằm mục đích giúp xác nhận số kiểm định (Number) do người sử dụng dịch vụ này cung cấp tương ứng với số kiểm định được lưu trữ tại nguồn dữ liệu của PNJL. Bảng tham khảo này không phải là giấy bảo hành, đánh giá hay định giá viên kim cương đính kèm và cũng không được xem như là một bảng giám định gốc của PNJL.</td></tr>
</table>
