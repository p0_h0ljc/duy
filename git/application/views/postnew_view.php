<div class="right_col">
    <div class="right-container">
      <div class="box">
        <div class="box-heading" >
          <i class="fa fa-cog"></i>
          <h3>Tất Cả Các Tin</h3>
          <div class="buttons">
            <a href="<?php echo base_url('adminnewA/add'); ?>" class="btn btn-sm btn-primary">Insert</a>
            <button class="btn btn-sm btn-danger" onclick="$('#form').submit()">Delete</button>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="box-body">
          <form id="form" action="<?php echo base_url('adminNewa/delete') ?>" method="post">
            <table class="table table-responsive table-bordered table-hover list_table "  >
            <thead>
              <tr>
                <td>
                  <input type="checkbox" onchange="$('input[name*=\'selected\']').prop('checked', this.checked);">
                </td>
                <td>Số</td>
                <td>Tiêu Đề </td>
              <!--   <td>Nội dung</td> -->
                <td>Ngày Đăng</td>
                <td>Chỉnh Sửa</td>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($news as $new) { 
              
              ?>
                <tr>
                  <td>
                    <input type="checkbox" value="<?php echo $new['id']; ?>" name="selected[]">
                  </td>
                  <td ><?php echo $new["id"] ?></td>
                  <td>
                    <?php echo $new["title"] ?>
                  </td>
 
                  <td ><?php echo $new["upload_date"]; ?></td>
                  <td class="center edit_link">
                    <a href="<?php echo base_url('adminnewA/view/' . $new["id"]); ?>">Edit</a>
                  </td>
                </tr>
              <?php } ?>
             
            </tbody>

          </table>
           <div class="row">
                    <div class="col-md-12 text-center">
                        <?php echo $paginator; ?>
                    </div>
                </div>
          </form>
          
        </div>
      </div>

    </div>
    
  </div>