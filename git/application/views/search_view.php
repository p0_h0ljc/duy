<div class="container content">
 	 
 		<?php foreach ($listproduct as $product) { 
								$image = explode(",", $product["images"])[0];
							?>
    	<div class="row">	
    	<div class="col-sm-2">
    		<a href="<?php echo base_url('product/'.$product['id']); ?>"><img class="img-responsive" src="<?php echo '/assets/images/products/' . $product['id'] . '/' . $image ?>"></a>
    	</div>
		<div class="col-sm-4" >
			<span style="font-size:25px;"><a href="<?php echo base_url('product/'.$product['id']); ?>"><?php echo $product["name"] ?></a></span>
				<div style="max-height: 100px;overflow: hidden;">
				<?php echo $product["description"] ?>
				</div>
		</div>
			<div class="col-sm-6" style="font-size:20px;color:red"><?php echo number_format($product["price"], 0, ',', '.'); ?>đ</div>
		</div>
		<hr>
<?php } ?>
</div>