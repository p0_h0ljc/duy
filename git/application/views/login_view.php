<script type="text/javascript">
	$('.toggle').on('click', function() {
  $('.container').stop().addClass('active');
});

$('.close').on('click', function() {
  $('.container').stop().removeClass('active');
});
</script>
<body>
 	<div class="container" style="  max-width: 460px;
  width: 100%;
  margin: 70px auto 100px;">
  
  <div class="card"></div>
  <div class="card">
    <h1 class="title">Login ADMIN</h1>
    <?php echo form_open("login/login"); ?>
      <div class="input-container">
        <input type="text" id="Username" required="required" name="username"/>
        <label for="Username">Username</label>
        <div class="bar"></div>
      </div>
      <div class="input-container">
        <input type="password" id="Password" required="required" name="password"/>
        <label for="Password">Password</label>
        <div class="bar"></div>
      </div>
      <div class="button-container" >
        <button><span>Log in</span></button>
      </div>
     
    <?php echo form_close(); ?>
  </div>
  
</div>