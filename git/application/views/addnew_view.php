<?php
header( 'Cache-Control: no-store, no-cache, must-revalidate' ); 
header( 'Cache-Control: post-check=0, pre-check=0', false ); 
header( 'Pragma: no-cache' ); 
?>
	<div class="right_col">
		<div class="right-container">
			<div class="box">
				<div class="box-heading">
					<i class="fa fa-cog"></i>
					<h3>Đăng Tin</h3>
				</div>
				<div class="box-body">
					<?php echo validation_errors('<p class="error">'); ?>
 		<form class="form-horizontal" enctype="multipart/form-data" id="form" action="<?php echo base_url('adminnewa/addnew') ?>" method="post">
            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="title">Chủ Đề: <i class="required">*</i></label>
              <div class="col-sm-6">
                <input class="form-control" type="text" id="title" name="title" required>
              </div>
              
            </div>
            
            <div class="form-group">
              <label class="control-label col-xs-12 col-sm-2" for="description">Nội Dung Bài Đăng: </label>
              <div class="col-sm-10">
                <div id="summernote"></div>
                <input type="hidden" name="description">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-2 col-sm-offset-10">
                <button type="submit" class="btn btn-primary btn-lg" onclick="return add();">  OK  </button>
              </div>
            </div>
          </form>
				</div>
			</div>

		</div>
		
	</div>
<script src="/vendors/summernote/dist/summernote.min.js"></script>
<script type="text/javascript">

	$(document).ready(function() {

	    // summernote
	    $('#summernote').summernote({
	    	height: 500,
	    	minHeight: 200,
	    	maxHeight: 800,
	    	focus: false,
	    	placeholder: 'Nội dung bài đăng'
	    });
	});


	function add() {
		var values = [];
		var isHasError = false;

		$('#form').find('span.errors').remove();

		// set data for input description
		$('#form').find('input[name="description"]').val($('#summernote').summernote('code'));

		$.each($('#form').serializeArray(), function(i, field) {
			
			switch (field.name) {
				case 'name': console.log(field);
					if (field.name == 'name' && field.value.length < 5) {
						$('#form').find('input[name="name"]').parent().parent().append("<span class='errors'>The length of title must be greater than 5 characters</span>");
						isHasError = true;
					}
					break;

				case 'price': console.log(field);
					if ( isNaN(parseFloat(field.value)) || field.value < 0) {
						$('#form').find('input[name="price"]').parent().parent().append("<span class='errors'>Price of product must be a number greater 0</span>");
						isHasError = true;
					}
					break;

				case 'quantity': console.log(field);
					if (parseInt(field.value) < 0) {
						$('#form').find('input[name="quantity"]').parent().parent().append("<span class='errors'>Quantity must be greater 0</span>");
						isHasError = true;
					}
					break;

				case 'brand_id': 
					if (field.value == '') {
						$('#form').find('select[name="brand_id"]').parent().parent().append("<span class='errors'>Please choose a brand</span>");
						isHasError = true;
					}
					break;

				default: 
					console.log("");
					break;
			}
			
		});
		

		if (!isHasError) {
			return true;
		}

		return false;
	}

</script>
