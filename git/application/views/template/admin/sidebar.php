<?php 
$this->load->helper("url");
$dashboardPage = base_url("admin/dashboard");
$productPage = base_url("adminproductview");
$addProductPage = base_url("admin/product/add");
$orderPage = base_url("admin/order");
$userPage = base_url("admin/users");
$guestPage = base_url("admin/guests");
$adminupload = base_url("admin/upload");
?>
	
<!DOCTYPE html>
<html>
<head>
	<title>Admin Page</title>

	<link rel="stylesheet" type="text/css" href="/vendors/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/vendors/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/vendors/summernote/dist/summernote.css">
	<link rel="stylesheet" type="text/css" href="/vendors/nprogress/support/style.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/management.css">
	<script type="text/javascript" src="/vendors/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/vendors/nprogress/nprogress.js"></script>
    
</head>
<body>
<div class="main_container">
	<div class="left_col">
		<div class="nav_title">
			<a class="site_title" href="<?php echo base_url('/homepage'); ?>">
				<i class="fa fa-paw"></i>
				<span>Brand</span>
			</a>
		</div>
		<div class="profile clearfix">
			<div class="profile_pic">
				<img src="/assets/images/profile_default.jpg">
			</div>
			<div class="profile_info">
				<span>Chào mừng admin</span>
				<h2> <?php echo $_SESSION['user_name']?> </h2>
			</div>
		</div>
		<div id="sidebar-menu">
			<div class="menu_section">
				<h3>Admin</h3>
				<ul class="nav side_menu">
					<li class="">
						<a>
							<i class="fa fa-home"></i>
							&nbsp; Trang Chủ
							<span class="fa fa-chevron-right"></span>
						</a>
						<ul class="nav child_menu">
							<li>
								<a href="<?php echo $dashboardPage; ?>">
									Dashboard
								</a>
							</li>
						</ul>
					</li>
					<li class="">
						<a>
							<i class="fa fa fa-plus-square"></i>
							&nbsp; Thêm Sản Phẩm 
							<span class="fa fa-chevron-right"></span>
						</a>
						<ul class="nav child_menu">
							<li>
								<a href="<?php echo $productPage; ?>">
									Tất Cả Sản Phẩm
								</a>
							</li>
							<li class="">
								<a href="<?php echo $addProductPage; ?>">
									Thêm
								</a>
							</li>
						</ul>
					</li>
					<li class="">
					<a>
						<i class="fa fa fa-diamond"></i>
						&nbsp; Đăng Tin Chủ Đề Kim Cương
						<span class="fa fa-chevron-right"></span>
					</a>
					<ul class="nav child_menu">
						<li>
							<a href="<?php  echo base_url('adminnewa');?>">
								Tất Cả Tin
							</a>
						</li>
						<li class="">
							<a href="<?php echo base_url('adminnewa/add')?>">
								Đăng Tin
							</a>
						</li>
					</ul>
					</li>
					<li class="">
					<a>
						<i class="fa fa fa-paw"></i>
						&nbsp; Đăng Tin Chủ Đề Đá Màu
						<span class="fa fa-chevron-right"></span>
					</a>
					<ul class="nav child_menu">
						<li>
							<a href="<?php  echo base_url('adminnewb');?>">
								Tất Cả Tin
							</a>
						</li>
						<li class="">
							<a href="<?php echo base_url('adminnewb/add')?>">
								Đăng Tin
							</a>
						</li>
					</ul>
					</li>
					<li class="">
						<a>
							<i class="fa fa-reply-all"></i>
							&nbsp; Bài đăng mục Tư vấn
							<span class="fa fa-chevron-right"></span>
						</a>
						<ul class="nav child_menu">
							<li>
								<a href="<?php echo base_url('adminnewa/view/26') ?>">
									Mục tư vấn
								</a>
							</li>
						</ul>
					</li>
					<li class="">
						<a>
							<i class="fa fa-video-camera"></i>
							&nbsp; Bài đăng mục Video
							<span class="fa fa-chevron-right"></span>
						</a>
						<ul class="nav child_menu">
							<li>
								<a href="<?php echo base_url('adminnewa/view/27') ?>">
									Mục Video
								</a>
							</li>
						</ul>
					</li>
					<li class="">
						<a>
							<i class="fa fa-users"></i>
							&nbsp; ADmin
							<span class="fa fa-chevron-right"></span>
						</a>
						<ul class="nav child_menu">
							<li>
								<a href="<?php echo $userPage; ?>">
									ADmin
								</a>
							</li>
						</ul>
					</li>
					<li class="">
						<a href="<?php echo $adminupload?>">
							<i class="fa fa-upload"></i>
							&nbsp; Đăng Tài Liệu
						</a>
					</li>
					<li class="">
						<a href="<?php  echo base_url('login/logout');?>">
							<i class="fa fa-sign-out"></i>
							&nbsp; Đăng Xuất
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>