</div>

<script type="text/javascript">
	$(document).ready(function() {

		$SIDEBAR_MENU = $('#sidebar-menu')
		
	    var currentUrl = window.location.href;

	    var link = $SIDEBAR_MENU.find(".child_menu a[href='" + currentUrl + "']");
	    if (link.length == 0) {
	    	$SIDEBAR_MENU.find("a[href='" + currentUrl + "']").parent().addClass("active");
	    } else {
	    	link.parent().addClass("current_page")
	 		.parent().parent().addClass("active").find("ul:first").slideDown();
	    }

	 	

		$SIDEBAR_MENU.find('a').on('click', function(ev) {
	        var $li = $(this).parent();
	        if ($li.is('.active')) {
	            $li.removeClass('active');
	            $('ul:first', $li).slideUp(function() {
	            });
	        } else {
	            // prevent closing menu if we are on child menu
	            if (!$li.parent().is('.child_menu')) {
	                $SIDEBAR_MENU.find('li').removeClass('active');
	                $SIDEBAR_MENU.find('li ul').slideUp();
	            }
	            
	            $li.addClass('active');
	            $('ul:first', $li).slideDown(function() {
	            });
	        }
	    });

	});

	if (typeof NProgress != 'undefined') {
	    $(document).ready(function () {
	        NProgress.start();
	    });

	    $(window).load(function () {
	        NProgress.done();
	    });
	}
</script>

</body>
</html>