
// Duy Header
function setupSlideMenu() {

	$('.menu-icon-mini').click(function() {
	 	$('.slide-menu').animate({
	      	left: "0px"
	    }, 1000, 'easeOutQuint');

	 	// disable vertical scrollbar on html but show sidebar
	 	$('html').toggleClass('noscroll');

	    // create wrapper for main div
	    var wrapper = $('<div class="wrapper"></div>');

	    // click on wrapper to close slide menu
	    wrapper.click(function() {
	    	$('.X-button').trigger('click');
	    });
	    $('.main').append(wrapper);

	    // animate wrapper's background
	    wrapper.fadeIn(1000);

	    $('.main').animate({	
	      	left: "250px"
	    }, 1000, 'easeOutQuint');
	 });

	$('.X-button').click(function() {

	  	// animate hide wrapper and remove it, also remove class noscroll on html to enable scroll
	  	$('.wrapper').fadeOut(200, function() {
	  		$('.wrapper').remove();
	  		$('html').toggleClass('noscroll');
	  	});

	    $('.slide-menu').animate({
	      	left: "-125px"
	   
	    }, 200);

	    $('.main').animate({
	      	left: "0px"
	    }, 200);
	  });
}


function setupFooter() {
	$(".li-ship").hover(
			function(){
				//alert('asd');
				$(this).find('img').attr('src', '../assets/images/free-ship-hover.png');
			},
			function(){
				$(this).find('img').attr('src', '../assets/images/free-ship.png');
			}
			);

		$(".li-ex").hover(
			function(){
				//alert('asd');
				$(this).find('img').attr('src', '../assets/images/exchange-hover.png');
			},
			function(){
				$(this).find('img').attr('src', '../assets/images/exchange.png');
			}
			);

		$(".li-sup").hover(
			function(){
				//alert('asd');
				$(this).find('img').attr('src', '../assets/images/sup-hover.png');
			},
			function(){
				$(this).find('img').attr('src', '../assets/images/sup.png');
			}
			);

		$(".li-gift").hover(
			function(){
				//alert('asd');
				$(this).find('img').attr('src', '../assets/images/gift-hover.png');
			},
			function(){
				$(this).find('img').attr('src', '../assets/images/gift.png');
			}
			);
			
		
		$('.scrollTop img').attr('title', 'Scroll Top');
		
			//Check to see if the window is top if not then display button
		/*$(window).scroll(function(){
			
			// get distance from current scroll bar to end of page
			var marginBottom = $(document).height() - ($(this).height() + $('body').scrollTop());
			
			if (marginBottom < 400) {
				$('.scrollTop').fadeIn();
			} else {
				$('.scrollTop').fadeOut();
			}
		});*/
		
		$(window).scroll(function() {
		if ($(this).scrollTop() > 200) {
		$('.scrollTop').fadeIn('slow');
		} else {
		$('.scrollTop').fadeOut('slow');
		}
		});
		
		$('.scrollTop').click(function (event) {
		event.preventDefault();
		$('html, body').animate({scrollTop: 0}, 300);
		});

	//Click event to scroll to top
	/*$('.scrollTop').click(function(){
		$('html, body').animate({scrollTop : 0},300);
		return false;
	});*/
	// Khôgn dùng đám này nữa
	// $('.login-form-box').click(function(event){
	// 		event.stopPropagation();
	// 		return true;
	// });
	// $('.login-form').click(function(){
	// 	$('.login-form').css('visibility', 'hidden');
	// 	$('body').css('overflow', 'visible');
	// });
	// // Hiện form log in khi ấn vào link log in
	// $('.log-in').click(function(){
	// 	$('.login-form').css('visibility', 'visible');

	// 	// $('body').css('overflow', 'hidden');
	// });
}

function setupProductDetailPage() {
	// setup image gallery
	var links = document.getElementById('links');
	
	if (!links) return;
	
	links.onclick = function (event) {
	    event = event || window.event;
	    var target = event.target || event.srcElement,
	        link = target.src ? target.parentNode : target,
	        options = {index: link, event: event, hidePageScrollbars: false},
	        links = this.getElementsByTagName('a');
	    blueimp.Gallery(links, options);
	};
  	
  	// setup share button
  	var shareBtn = new ShareButton({
  		url: 'http://assignment1.webprogramming.hcmut.edu.vn'
  	});
}

// scroll to a element with its id
function scrollTo(eleId) {
	var target = $(eleId);
	$('html, body').stop().animate({
		'scrollTop': target.offset().top
	}, 500, 'easeInOutCubic');
}

// open login form, just call this function
function open_login_form(){
	$('.login-form').css('visibility', 'visible');
	// $('body').css('overflow', 'hidden');
}
// Dùng để đóng cái login form
function cpu(){
		$('.login-form').css('visibility', 'hidden');
		// $('body').css('overflow', 'visible');
}

function setupFloatInput() {
	$("input.float_input").on("focus", function() {
		$(this).prev("label.float_label").addClass("filled focused");
	});

	$("input.float_input").on("blur", function() {
		$(this).prev("label.float_label").removeClass("focused");
		if (this.value === '') {
			$(this).prev("label.float_label").removeClass("filled");
		}
	});
}



var main = function() {
	setupSlideMenu();
	setupFooter();
	setupProductDetailPage();
	setupFloatInput();
};

// bootstrap app
$(document).ready(main);


