-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2016 at 04:06 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop_computer`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` int(2) NOT NULL,
  `name` varchar(100) NOT NULL,
  `logo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `name`, `logo`) VALUES
(1, 'Dell', '/assets/images/dell_logo.svg'),
(2, 'HP', 'hp_logo.svg'),
(3, 'Asus', '/assets/images/asus-logo.png'),
(4, 'Acer', '/assets/images/acer-logo.png'),
(5, 'Lenovo', '/assets/images/lenovo-logo.gif');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `total` decimal(10,0) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address` varchar(200) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `total`, `user_id`, `address`, `date_created`) VALUES
(1, '98550000', 4, 'Quan 10, TPHCM', '2016-05-09 23:56:25'),
(2, '230300000', 7, 'xxx', '2016-05-10 00:53:29'),
(3, '66600000', 7, 'thpHCM', '2016-05-10 01:22:12');

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(2) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`id`, `product_id`, `quantity`, `order_id`) VALUES
(1, 24, 7, 1),
(2, 9, 1, 1),
(3, 10, 1, 2),
(4, 18, 6, 2),
(5, 6, 1, 2),
(6, 23, 5, 2),
(7, 9, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `cpu` varchar(100) NOT NULL,
  `memory` varchar(10) NOT NULL,
  `hard_drive` varchar(100) NOT NULL,
  `video_card` varchar(100) NOT NULL,
  `display` varchar(100) NOT NULL,
  `dimensions` varchar(50) NOT NULL,
  `wireless` varchar(100) NOT NULL,
  `battery` varchar(50) NOT NULL,
  `quantity` int(4) NOT NULL,
  `images` text NOT NULL,
  `name` varchar(64) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `description` text NOT NULL,
  `brand_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `cpu`, `memory`, `hard_drive`, `video_card`, `display`, `dimensions`, `wireless`, `battery`, `quantity`, `images`, `name`, `price`, `description`, `brand_id`) VALUES
(6, 'Intel(R) core i5-6200U (2.3GHz upto 2.8GHz, 4Thread, 3M cache, BUS 1600MHz)', '8GB', '128GB SSD Sata3', 'Intel HD Graphic 530 ', '13.3" FHD IPS LED', '1920x1080', '1000Mbps / WLan 802.11b,g,n / Bluetooth', 'gần 10h', 11, '1.jpg,2.jpg,3.jpg,4.jpg', 'Dell XPS 13 - 58G128S', '23000000', 'Xứng đáng là siêu phẩm bậc nhất của Dell, Dell XPS 13 mang trong mình một thiết kế vô cùng cao cấp từ chất liệu cho đến kiểu dáng. Sự kết hợp hoàn hảo giữa hợp kim magiê và lớp vỏ nhôm màu bạc sang trọng cùng sợi carbon được dệt tinh tế bao phủ khắp các mặt máy đã tạo nên một phong cách pha trộn độc đáo, cho cảm giác rất êm ái, mịn màng và thanh thoát đồng thời lại giúp máy cực kỳ chắc chắn, bền bỉ và chịu được áp lực cao trong mọi hoàn cảnh.', 1),
(7, 'Intel(R) core i5-5200U (2.2GHz upto 2.7GHz, 4Thread, 3M cache, BUS 1600MHz)', '6GB', '500GB 5400RPM SATA', 'AMD Raedon HD R7 M270 4GB + Intel Graphic HD 5500', '15.6" HD LED', '1366x768', '1000Mbps | Wifi 802.11b,g,n | Bluetooth', 'gần 4 giờ', 22, '1.jpg,2.jpg', 'Dell inspiron N7548 - 70055069', '16900000', 'Sự kết hợp hài hòa giữa tất cả các chi tiết, từ thiết kế sang trọng đến màn hình lớn 15.6 inch sắc nét bên cạnh cấu hình tuyệt đỉnh với bộ vi xử lý Broadwell Core i7 đi kèm RAM 8GB và đặc biệt là card đồ họa rời 4GB bên trong cùng nhiều tiện ích mới mẻ đã giúp Dell N7548 trở thành một thiết bị hoàn hảo trong cả công việc, giải trí cũng như thiết kế đồ họa.', 1),
(8, 'Intel(R) Core i3-6200U (2.3GHz upto 2.8GHz, 4Thread, 3MB cache, BUS 1600MHz)', '8GB', '1TB 5400RPM SATA', 'AMD R5 M335 2GB + Intel HD Graphic 520', '14.1" HD LED', '1366x768', '1000Mbps | Wifi 802.11b,g,n | Bluetooth', 'gần 3.5giờ', 12, '2.jpg,1.jpg,3.jpg', 'Dell Inspiron N5459-WX9KG2', '15800000', 'Hàng chính hãng, Mới 100%, đầy đủ phụ kiện. Bảo hành 12 Tháng theo tiêu chuẩn NSX. Đổi mới trong 15 ngày đầu nếu bị lỗi phần cứng NSX.', 1),
(9, 'Intel® Core™ i5-6200U Processor (3M Cache, up to 2.80 GHz)', '4GB', '500GB 5400RPM SATA', 'Intel HD Graphic 520 + Nvidia GT930M 2GB', '14.1" HD LED', '1366x768', '100Mbps | Wifi 802.11b,g,n | Bluetooth', 'gần 3.5giờ', 21, '1.jpg,2.jpg,3.jpg,4.jpg', 'Dell Inspiron N2679-JXCKS2', '16650000', 'Hàng chính hãng, Mới 100%, đầy đủ phụ kiện. Bảo hành 12 Tháng theo tiêu chuẩn NSX. Đổi mới trong 15 ngày đầu nếu bị lỗi phần cứng NSX.', 1),
(10, 'Intel® Core™ i7-5500U Processor (4M Cache, up to 3.00 GHz)', '4GB', '4GB', 'Nvidia Geforce GT 820M 2GB + Intel Graphics HD 5500', '14.1" HD LED', '1366x768', 'Wifi 802.11b,g,n | Bluetooth', 'gần 3giờ', 14, '1.jpg,2.jpg,3.jpg,4.jpg', 'Dell inspiron N3443 - C4I71820', '13550000', 'Bảo hành 12 Tháng theo tiêu chuẩn NSX. Đổi mới trong 15 ngày đầu nếu bị lỗi phần cứng NSX.', 1),
(11, 'Intel(R) core i7-5500U (2.4GHz ~ 3.0GHz, 4Thread, 4M cache, BUS 1600MHz)', '4GB', '500GB 5400RPM SATA', 'AMD Radeon R5 M330 2GB + Intel® HD Graphics 5500', '15.6" HD LED', '1366x768', '100Mbps / WLan 802.11b,g,n / Bluetooth', '4cell Polymer', 12, '1.jpg,2.jpg,3.jpg', 'Hp 15-ac104TX', '15500000', 'Bảo hành 12 Tháng theo tiêu chuẩn NSX. Đổi mới trong 15 ngày đầu nếu bị lỗi phần cứng NSX.', 2),
(12, 'Intel(R) core i7-6500U (2.5GHz upto 3.1GHz, 4Thread, 4M cache, BUS 1600MHz)', '8GB', '256Gb SSD', 'Intel® HD Graphics 520', '13.3" QHD', 'QHD', '1000Mbps / WLan 802.11b,g,n / Finger', '6cell Li-Ion Battery', 21, '1.jpg,2.jpg,3.jpg,4.jpg', 'Hp Envy 13-d019TU', '23250000', 'Sau bao nhiêu thế hệ, HP Envy 15 vẫn giữ được sức hấp dẫn vốn có của dòng HP Envy với đường nét tinh tế và chăm chút trong từng chi tiết, các góc cạnh bo tròn, kiểu dáng mỏng dần về phía trước luôn mang lại cảm giác nhẹ nhàng nhưng tinh tế. Mang trong mình linh hồn của dòng Envy, HP Envy 15 được thiết kế hướng đến nhu cầu giải trí và chất lượng hoàn hảo cao. Khung máy được làm từ hợp kim với lớp vỏ nhôm tạo nên cảm giác chắn. Đúng với chất Envy, tuy vỏ nhôm nhưng không hề bám vân tay nhiều như các máy khác.', 2),
(13, 'Intel(R) core i7-5500U (2.4GHz upto 3.0GHz, 4Thread, 3M cache, BUS 1600MHz)', '8GB', '1TB 5400RPM SATA', 'ATI Mobility Radeon™ R5 M255 2GB + Intel HD 5500', '15.6" HD wide LED', '1366x768', '1000Mbps / WLan 802.11b,g,n / Bluetooth', '4cell Li-Ion Battery', 6, '1.jpg,2.jpg,3.jpg', 'Hp Probook 450 G2 - M1V32PA ', '19600000', 'HP mới đây đã làm mới dòng ProBook của mình với những chiếc thuộc dòng 400 với kích thước màn hình từ 13,3" đến 17,3". So với thế hệ trước, những chiếc ProBook 4xx mỏng hơn 35%, nhẹ hơn 18% và có thiết kế mới. Tất cả những thiết bị mới đều được HP sơn màu "xám thiên thạch" mà theo hãng là sẽ giúp máy trông vẫn đẹp sau một thời gian dài sử dụng. ProBook Series 400 đã vượt qua "bài kiểm tra độ bền trong 115.000 giờ" và được chứng nhận kĩ lưỡng để phù hợp cho môi trường sử dụng trong các doanh nghiệp nhỏ. Các model trong đợt ra máy nào bao gồm HPProBook 430 (13,3"), ProBook 440/445 (14"), ProBook 450/455 (15") và ProBook 470 (17,3").', 2),
(14, 'Intel(R) core i7-5500U (2.4GHz ~ 3.0GHz, 4Thread, 4M cache, BUS 1600MHz)', '4GB', '500GB 5400RPM SATA', 'AMD Radeon R5 M330 2GB + Intel® HD Graphics 5500', '15.6" HD LED', '1366x768', '100Mbps / WLan 802.11b,g,n / Bluetooth', '4cell Polymer', 5, '1.jpg,2.jpg,3.jpg', 'Hp 15-ac104TX ', '15500000', 'Bảo hành 12 Tháng theo tiêu chuẩn NSX. Đổi mới trong 15 ngày đầu nếu bị lỗi phần cứng NSX.', 2),
(15, 'Intel(R) core i7-6500U (2.5GHz upto 3.1GHz, 4Thread, 4M cache, BUS 1600MHz)', '8GB', '1TB 5400RPM SATA', 'Nvidia Geforce GT 940M 2GB +Intel® HD Graphics 520', '15.6"', '1920x1080', '1000Mbps / WLan 802.11b,g,n / Finger', '6cell Li-Ion Battery', 3, '1.jpg,2.jpg,3.jpg,4.jpg', 'Hp Envy 15 - ae130TX ', '23200000', '-Thiết kế đẹp, phù hợp cả nam và nữ.\r\n-Hoàn thiện tốt.\r\n-Loa hay.\r\n-Công nghệ Coolsense tản nhiệt êm ái nhưng mát.\r\n-Gói bảo hành VIP.', 2),
(16, 'Intel(R) core i7-6700HQ (2.6GHz upto 3.5GHz, 8Thread, 6M cache, BUS 2133MHz)', '8GB', '128GB SSD + 1TB 7200RPM SATA', 'Nvidia Geforce GTX 960M 4GB GDDR5 + Intel HD 530', '17.3" FHD LED', '1920x1080', '1000Mbps | Wifi 802.11b,g,n | Bluetooth', 'gần 4giờ', 22, '1.jpg,2.jpg,3.jpg,4.jpg', 'ASUS GL752VW - T4163D', '28000000', 'Laptop ROG G752 sở hữu chip xử lý thế hệ thứ 6 Intel® Skylake Core™ i7  cùng với card đồ họa NVIDIA GeForce GTX 960M, hỗ trợ Microsoft DirectX 12. Hãy trải nghiệm game hoặc chạy các ứng dụng với hiệu suất và hình ảnh đáng kinh ngạc.', 3),
(17, 'Intel(R) Core™ M-6Y30 (0.9GHz upto 2.2GHz, 4Thread, 4M cache, BUS 1600MHz)', '8GB', '128GB SSD Sata3', 'Intel® HD Graphics 515', '13.3" FHD IPS LED', '1920x1080', 'Wifi 802.11b,g,n | Bluetooth', 'gần 8giờ', 4, '1.jpg,2.jpg,3.jpg,4.jpg', 'Asus UX305CA - FC036T', '17000000', 'Để khẳng định thương hiệu và vị trí của mình, ASUS luôn cải tiến và nâng cấp mẫu mã cũng như chất lượng với các sản phẩm tiện ích cao cấp như Zenbook UX301LA, UX303LN, thì mới đây hãng Asus đã giới thiệu 1 phiên bản mới UX305FA với màn hình FHD máy chỉ mỏng 12.3mm thuộc hàng mỏng nhất trong dòng laptop 13inch hiện nay và trọng lượng chỉ có 1.2 kg và vì dùng chip xử lý mới Core M nên máy có thể hoạt động lên đến 10 tiếng.\r\n\r\nNếu bạn đang muốn tìm một chiếc laptop mang tính siêu di động, gọn nhẹ đẹp thì UX305FA là một chiếc máy tuyệt vời với lựa chọn của bạn', 3),
(18, 'Intel(R) core i7-6500U (2.5GHz upto 3.1GHz, 4Thread, 4M cache, BUS 1600MHz)', '4GB DDR3L', '1TB 5400RPM SATA3', 'Nvidia Geforce GT 930 2GB + Intel® HD Graphics 520', '15.6" HD LED', '1366x768', '100Mbps | Wifi 802.11b,g,n', 'gần 3.5giờ', 7, '1.jpg,2.jpg,3.jpg,4.jpg', 'Asus A556UF - XX063D', '15250000', 'Asus A556UF-XX063D là một chiếc máy tính xách tay có thiết kế hiện đại và linh hoạt. Máy còn được trang bị màn hình lớn 15,6 inch và một bộ vi xử lý vô cùng mạnh mẽ đi kèm card đồ họa rời 2GB giúp đáp ứng hoàn hảo cho cả nhu cầu công việc hàng ngày cũng như giải trí với các game 3D nặng. Sản phẩm phù hợp cho mọi đối tượng sử dụng, từ những người làm việc văn phòng đến game thủ lẫn những người làm thiết kế, đồ họa,…', 3),
(19, 'Intel(R) core i5-4210M (2.6GHz upto 3.2GHz, 4Thread, 3M cache, BUS 1600MHz)', '4GB', '500GB 7200RPM SATA3', 'Nvidia Geforce GT 930M 2GB + Intel Graphic HD 4400', '14.1" FHD LED ', '1920x1080', '1000Mbps | Wifi 802.11b,g,n | Bluetooth', 'gần 3.5giờ', 2, '1.jpg,2.jpg,3.jpg,4.jpg', 'Asus Pro PU451JF - FA056D', '13750000', 'Với những dòng sản phẩm laptop đến từ hãng Asus thì hầu như mình đã sử dụng thử rất nhiều các dòng máy khác nhau từ bạn bè haynguo72i thân, chẳng hạn như các dòng K series hoặc X series lànhững dòng sản phẩm rất được các bạn học sinh sinh viên quan tâm chú ý đến, hay dòng N series dành cho những đối tượng chú trọng đến tính năng giải trí đa phương tiện, dòng UX series dành chonhững đối tượng thích sự linh động hoặc dòng G series là dòng sản phẩm dành riêng cho các đối tượng game thủ với kiểu dáng thiết kế hầm hố cùng hiệu năng đầy mạnh mẽ. Tuy nhiên, ít ai biết rằng Asus vẫn còn một dòng sản phẩm khác mà hãng hướng đến những đối tượng là những doanh nhân hay những người dùng có túi tiền khiêm tốn, nhưng vẫn mong muốn có một chiếc laptop không cầu kỳ về kiểu dáng thiết kế mà vẫn đảm bảo một hiệu năng mạnh mẽ, độ bền bỉ cùng khả năng bảomật cá nhân cao, và đó chính là chiếc AsusPro PU451JF mà mình muốn chia sẻ đến các bạn.', 3),
(20, 'Intel(R) core i5-6200U (2.3GHz Upto 2.8Ghz, 4Thread, 3M cache, BUS 1600MHz)', '4GB', '500GB 5400RPM SATA3', 'Intel HD Graphic 520', '15.6" FHD Touch', '1920x1080', '1000Mbps / WLan 802.11b,g,n', '2Cell 38Whr', 4, '1.jpg,2.jpg,3.jpg', 'Asus TP501UA-DN024T', '13200000', 'Bảo hành 24 Tháng theo tiêu chuẩn NSX. Đổi mới trong 15 ngày đầu nếu bị lỗi phần cứng NSX. Khuyến mãi	Balo cao cấp, Chuột quang, Túi chống sốc.', 3),
(21, 'Intel(R) i5-6300HQ (2.3GHz upto 3.2GHz , 4Thread, 6M cache, BUS 2133MHz)', '8GB', 'mSSD 8GB + 1TB HDD 5400 RPM', 'Nvidia Geforce GTX 960M 4GB GDDR5 + Intel HD Graphic 530', '15.6" FHD IPS', '1920x1080', '1000Mbps | Wifi 802.11b,g,n | Bluetooth', 'gần 4giờ', 2, '1.png,2.jpg,3.jpg,4.jpg', 'Acer Nitro BE VN7-592G-52TG', '19900000', 'Aspire V 15 Nitro Black Edition thuộc dòng sản phẩm laptop hướng đến game thủ, trang bị cấu hình phần cứng mạnh không kém máy tính để bàn với màn hình 15,6 inch độ phân giải Full HD, chip Haswell Core i7-4710HQ và đặc biệt là đồ họa rời cao cấp nhất dòng GeForce 800M series.\r\nBên cạnh đó, V 15 Nitro còn có thiết kế mỏng nhẹ tương đương laptop tiêu chuẩn với độ mỏng chỉ 24 mm và cân nặng 2,4 kg, thích hợp cho những chuyến đi du đấu cùng bạn bè. Điểm đáng tiếc là thời lượng dùng pin của máy ở mức trung bình và độ nhạy bàn phím chưa thật sự tốt như một số mẫu laptop chuyên game mà chúng tôi từng thử nghiệm.', 4),
(22, 'Intel(R) i7-6700HQ (2.6GHz upto 3.5GHz , 8Thread, 6M cache, BUS 2133MHz)', '8GB', 'mSSD 8GB + 1TB HDD 5400 RPM', 'Nvidia Geforce GTX 960M 4GB GDDR5 + Intel HD Graphic 530', '15.6" FHD IPS', '1920x1080', '1000Mbps | Wifi 802.11b,g,n | Bluetooth', 'gần 4giờ', 2, '1.png,2.jpg,3.jpg,4.jpg', 'Nitro BE VN7 - 592G - 77DU', '24400000', 'Thiết kế đẹp, kiểu dáng mỏng nhẹ, tính di động linh hoạt.\r\nHiệu năng tổng thể và khả năng xử lý đồ họa tốt.\r\nHỗ trợ đầy đủ các cổng kết nối phổ dụng.\r\nBàn phím full size, tích hợp đèn nền LED.\r\nÂm thanh tốt, âm lượng lớn, sống động và chi tiết.', 4),
(23, 'Intel® Core™ i7-6500U Processor (4M Cache, up to 3.10 GHz)', '4GB', '128GB SSD', 'Intel® HD Graphics 520', '14" FHD LED', '1920x1080', '1000Mbps / WLan 802.11b,g,n / Bluetooth', '4cell polymer (>4h)', 3, '1.jpg,2.jpg,3.jpg,4.jpg', 'Acer R5-471T-7387', '20450000', 'Thiết kế đẹp, kiểu dáng mỏng nhẹ, tính di động linh hoạt.\nHiệu năng tổng thể và khả năng xử lý đồ họa tốt.\nHỗ trợ đầy đủ các cổng kết nối phổ dụng.\nBàn phím full size, tích hợp đèn nền LED.\nÂm thanh tốt, âm lượng lớn, sống động và chi tiết.', 4),
(24, 'Intel® Core™ i5-6200U Processor (3M Cache, up to 2.80 GHz)', '4GB', '8GB SSD + 500GB HDD', 'Intel® HD Graphics 520', '13.3" HD LED', '1366x768', '1000Mbps / WLan 802.11b,g,n / Bluetooth', '4cell polymer (>4h)', 8, '1.jpg,2.jpg,3.jpg,4.jpg', 'Acer V3-372-59AB', '11700000', 'Laptop Acer Aspire V3 371 phiên bản Core i5 mang đến cho bạn một thiết bị cao cấp, thiết kế đẹp nhờ mặt lưng chất liệu nhôm, cấu hình mạnh mẽ, công nghệ thu sóng WiFi MiMo và pin sử dụng đến 6.5 giờ. Thiết kế máy là sự kết hợp hài hòa giữa chất liệu nhựa và nhôm. Phần nắp lưng, máy sử dụng chất liệu nhôm có các chi tiết lõm hình lục giác chống bám dấu vân tay hiệu quả. Vỏ kim loại sáng bóng giúp máy thêm phần sang trọng, độ hoàn thiện tốt càng khiến sản phẩm chắc chắn, cứng cáp. Nhờ chất liệu kim loại, máy có khả năng tản nhiệt tốt hơn. ', 4),
(25, 'Intel(R) i5-4210M (2.6GHz Upto 3.2GHz , 4Thread, 3M cache, BUS 1600MHz)', '4GB', '1TB HDD 5400RPM', 'Nvidia Geforce GT 840M 2GB + Intel HD Graphic 4600', '1000Mbps / WLan 802.11b,g,n / Bluetooth', '15.6" HD LED', '1366x768', '6cell polymer (>3h)', 5, '1.jpg,2.jpg,3.jpg', 'Laptop Acer E5-572G-59QZ', '11250000', 'Thiết kế đẹp, kiểu dáng mỏng nhẹ, tính di động linh hoạt.\r\nHiệu năng tổng thể và khả năng xử lý đồ họa tốt.\r\nHỗ trợ đầy đủ các cổng kết nối phổ dụng.\r\nBàn phím full size, tích hợp đèn nền LED.\r\nÂm thanh tốt, âm lượng lớn, sống động và chi tiết.', 4),
(26, 'Intel(R) core i5-4300U (1.9GHz upto 2.9GHz, 4Thread, 3M cache, BUS 1600MHz)', '8GB ', '250GB SSD SATA', 'Intel Graphic HD 4400', '14.1" HD LED', '1366x768', '1000Mbps | Wifi 802.11b,g,n | Bluetooth', 'gần 6giờ', 6, '1.jpg,2.jpg,3.jpg,4.jpg', 'IBM ThinkPad T440 - 548G25S', '16000000', 'Với thời lượng pin tốt, bàn phím chất lượng cao và hiệu năng xử lý mạnh mẽ, có thể nói bạn sẽ khó tìm được một chiếc laptop làm việc nào tốt hơn ThinkPad T440s.\r\nTừ lâu, dòng sản phẩm ThinkPad T của Lenovo đã được mệnh danh là dòng sản phẩm hàng đầu dành cho các có nhu cầu cần sức mạnh phần cứng cao. ThinkPad T440s vẫn tiếp tục truyền thống của T Series với nhiều ưu điểm về máy có thời lượng pin rất tốt, hiệu năng xử lý mạnh mẽ, bàn phím linh hoạt và màn hình cảm ứng 1080p (trên một số phiên bản). Tuy vậy, mức giá khởi điểm 1.199 USD (giá gốc tại Mỹ, tương đương 25,5 triệu đồng) là quá cao với đa số người dùng.', 5),
(27, 'Intel® Core™ i5-6200U Processor (3M Cache, up to 2.80 GHz)', '8GB', '192GB SSD SATA', 'Intel® HD Graphics 520', '14.1" FHD LED', '1920x1080', '1000Mbps | Wifi 802.11b,g,n | Bluetooth', 'gần 6giờ', 6, '1.jpg,2.jpg,3.jpg,4.jpg', 'Thinkpad T460s - 20FA0013VA', '24000000', 'ThinkPad T440s là người kế thừa hoàn hảo của T431s. Với sức mạnh và khả năng tiết kiệm điện của vi xử lý Haswell, thời lượng pin "khủng" và bàn phím có chất lượng tốt nhất trên thị trường, ThinkPad T440s đượcLaptopMag đánh giá là chiếc laptop doanh nhân tốt nhất của năm 2013. Nếu bạn muốn mua một chiếc laptop thực sự nhỏ nhắn, bạn có thể cân nhắc model ThinkPad X240 màn hình 12.5 inch của Lenovo. Tuy vậy, nếu bạn muốn mua một chiếc laptop để phục vụ cho công việc, ThinkPad T440s là một sự lựa chọn hợp lý cho bạn.', 5),
(30, 'Intel(R) core i3-3110M (2.4GHz, 4Thread, 3M cache, BUS 1600MHz)', '2GB', '500GB 5400RPM SATA', 'Intel Graphic HD 4000', '14.1" HD LED', '1366x768', '100Mbps | Wifi 802.11b,g,n | Bluetooth', 'gần 3giờ', 9, '1.jpg,2.jpg,3.jpg,4.jpg', 'Lenovo G400s - 59391069', '8150000', 'Lenovo Essential G480 – notebook giá tốt mùa Giáng sinh\r\nChiếc máy tính này mang đến sự phá cách trong thiết kế qua những đường nét mềm mại và thanh thoát đi cùng cấu hình mạnh, tích hợp nhiều công nghệ tiên tiến.\r\nẤn tượng đầu tiên là sự thay đổi đáng kể về kiểu dáng của Lenovo Essential G480 với màu đen nhám bạc bóng mượt, các góc bo tròn khác hoàn toàn với vẻ góc cạnh của dòng máy tính xách tay doanh nghiệp truyền thống. Đây cũng là xu hướng thiết kế mới của dòng laptop phổ thông Lenovo trong thời gian tới. Vỏ máy và mặt trong được sơn đen nhám bạc ánh kim, phủ một lớp nhựa láng bóng vân chìm. Lenovo Essential G480 có vẻ ngoài cứng cáp nhưng vẫn mang nét mềm mại, thời trang.', 5);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `user_type` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `phone_number`, `address`, `first_name`, `last_name`, `user_type`) VALUES
(3, 'guest1', '', '', '0972156406', 'Quan 10, TPHCM', 'Gien', 'Tran', 2),
(4, 'anhgien', '$2y$10$P5t5IP7GRXq9tOtvbqoho.m9wEBQQqhJ3O6wxARqqb7Zk0aSAKhWu', 'giencntt@gmail.com', '0987654321', 'quan 10, tphcm', 'Gien', 'Tran', 1),
(5, '', '', 'abc@gmail.com', '1234567890', 'TPHCM', 'Gien', 'Tran', 2),
(7, 'admin', '$2y$10$BacZXEQpbQq5myJhtONztOhovJ7WQImn3opmrtMZHqYzCsjv5I7py', '123456', NULL, NULL, 'Tran', 'Gien', 0),
(8, 'anhgien3', '$2y$10$/OZNbbt3Ca75RI08fmor7OPq9kSlZ8WYX0kaRlSEdxemccgpmfFIS', 'abc123@gmail.com', '90930393040', 'Binh Dinh', 'Gien', 'Trann', 1),
(9, 'duyduy', '$2y$10$qtyhMgDUtTjrnIZCyBCljemfQiVpKUVRvuhOBpMnrsco7nm5EQDrK', 'aaaaad@gmail.com', '0987657386', 'TPHCM', 'duy', 'tran', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_ibfk_1` (`user_id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_item_ibfk_2` (`order_id`),
  ADD KEY `order_item_ibfk_1` (`product_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `brand_id` (`brand_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`,`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_item`
--
ALTER TABLE `order_item`
  ADD CONSTRAINT `order_item_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_item_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
